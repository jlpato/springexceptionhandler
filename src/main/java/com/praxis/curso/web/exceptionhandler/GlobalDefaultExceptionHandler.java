package com.praxis.curso.web.exceptionhandler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {

	  @ExceptionHandler(value = Exception.class)
	  public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e)
			  throws Exception {
	    ModelAndView mav = new ModelAndView();
	    mav.addObject( "errorMsg", "Ocurrió un error al procesar tu solicitud: " + e.getMessage() );
	    mav.setViewName("error");
	    return mav;
	  }
	  
	  @ExceptionHandler(value = ArithmeticException.class)
	  public ModelAndView aritmeticErrorHandler(HttpServletRequest req, Exception e)
			  throws Exception {
	    ModelAndView mav = new ModelAndView();
	    mav.addObject( "errorMsg", "Ocurrió un error aritmetico al procesar tu solicitud: " + e.getMessage() );
	    mav.setViewName("arithmeticError");
	    return mav;
	  }
	  
}
