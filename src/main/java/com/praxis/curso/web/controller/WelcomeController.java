package com.praxis.curso.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

@Controller
public class WelcomeController {

    private final Logger logger = LoggerFactory.getLogger(WelcomeController.class);

    @GetMapping("/")
    public String index(Model model) {
        logger.debug("Welcome to com.praxis.curso...");
        model.addAttribute("msg", getMessage());
        model.addAttribute("today", new Date());
        return "index";

    }

    private String getMessage() {
        return "Hello World";
    }

    @GetMapping("/wrong")
    public String wrong(Model model) {
        logger.debug("Aqui vamos a provocar un error...");
        String valorNulo = null;
        valorNulo.toString();
        logger.debug( "Esto no se va a mostrar nunca.");
        return "wrong";

    }

    @GetMapping("/byzero")
    public String byzero(Model model) {
        logger.debug("Aqui vamos a provocar un error...");
        float valor = 0/0;
        logger.debug( "Esto no se va a mostrar nunca.");
        return "wrong";

    }

}
