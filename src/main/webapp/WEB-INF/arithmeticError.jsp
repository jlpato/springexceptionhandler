<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<img alt="" src="https://i.kym-cdn.com/photos/images/original/000/787/356/d6f.jpg" style="width: 100px"/>
<br/>
<h1>Ocurri&oacute; un error aritm&eacute;tico.</h1>

<c:if test="${ not empty errorMsg }">
	<c:out value="${ errorMsg }"/>
</c:if>

</body>
</html>