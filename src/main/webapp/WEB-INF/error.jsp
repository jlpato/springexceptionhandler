<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<img alt="" src="https://i0.wp.com/erizos.mx/wp-content/uploads/2018/08/meme-chavo-del-ocho-sorprendido.png" style="width: 100px"/>
<br/>
<h1>Ocurri&oacute; un error.</h1>

<c:if test="${ not empty errorMsg }">
	<c:out value="${ errorMsg }"/>
</c:if>

</body>
</html>